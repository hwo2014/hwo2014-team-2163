class SpeedObserver:
    def __init__(self, car_positions, track):
        self.car_positions = car_positions
        self.track = track
        self.pending_updates = {}
        for car_id in self.car_positions.car_map.keys():
            self.pending_updates[car_id] = PendingUpdate(self.track)

    def update_speed_info(self):
        for car_id, car_pos in self.car_positions.car_map.iteritems():
            self.pending_updates[car_id].update(car_pos)

    def crash(self, car_id):
        self.pending_updates[car_id].crash()


class PendingUpdate:
    def __init__(self, track):
        self.current_location = None
        self.speed = 0
        self.track = track

    def update(self, car_position):
        if (self.current_location != None and
            car_position.location.piece_index !=
            self.current_location.piece_index):
            self.track.update_fastest_speed_seen(self.current_location,
                                                 self.speed)
            self.speed = 0
        else:
            if car_position.speed > self.speed:
                self.speed = car_position.speed
        self.current_location = car_position.location

    def crash(self):
        print "crash at piece {0} @ {1}".format(
            self.current_location.piece_index, self.speed)
        self.track.update_lowest_crash_speed_seen(self.current_location,
                                                  self.speed)
        self.speed = 0
