import unittest
import mock
import copy
from main import KekeBot
from socket import socket


class TestMeasurements(unittest.TestCase):
    SAMPLE_TRACK = {'race':
        {
          'track': {
            'pieces': [
                {'length': 100.0},
                {'length': 100.0, 'switch': True},
                {'angle': 45.0, 'radius': 100},
                {'angle': 45.0, 'radius': 100},
                {'angle': 22.5, 'switch': True, 'radius': 200},
                {'angle': -45.0, 'radius': 100},
                {'length': 90.0}
            ],
            'lanes': [
                {'index': 0, 'distanceFromCenter': -10},
                {'index': 1, 'distanceFromCenter': 10}
            ],
          },
          'cars': [
            {
              'id': {
                'name': 'Schumacher',
                'color': 'red'
              },
              'dimensions': {
                'length': 40.0,
                'width': 20.0,
                'guideFlagPosition': 10.0
              }
            },
            {
              'id': {
                'name': 'Keke',
                'color': 'blue'
              },
              'dimensions': {
                'length': 40.0,
                'width': 20.0,
                'guideFlagPosition': 10.0
              }
            },
          ]
        }
    }
    LAST_PIECE = 6

    def setUp(self):
        self.socket = mock.create_autospec(socket)
        self.name = "Keke"
        self.mycolor = "red"
        self.other_color = "blue"
        self.keke = KekeBot(self.socket, self.name, "key")
        self.keke.on_your_car({"color": self.mycolor, "name": self.name})
        self.keke.on_game_init(self.SAMPLE_TRACK)
        self.set_location(0, 0, 0)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_speed(self):
        self.set_location(0, 10, 0)
        self.assertEqual(self.keke.get_speed(), 10)

    def test_speed_when_piece_changed(self):
        self.set_location(0, 10, 0)
        self.set_location(1, 10, 0)
        self.assertEqual(self.keke.get_speed(), 100)

    def test_speed_when_lap_changed(self):
        self.set_location(self.LAST_PIECE, 10, 0)
        self.set_location(1, 10, 0)
        self.assertEqual(self.keke.get_speed(), 190)

    def test_own_car_identified_by_color(self):
        pos_other1 = {'piecePosition':
                   {'pieceIndex': 0,
                    'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                    'lap': 0,
                    'inPieceDistance': 0},
                 'angle': 0, 'id': {'color': self.other_color,
                                    'name': self.name}}
        pos_me1 = {'piecePosition':
                   {'pieceIndex': 0,
                    'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},
                    'lap': 0,
                    'inPieceDistance': 0},
                 'angle': 0, 'id': {'color': self.mycolor, 'name': self.name}}
        self.keke.on_car_positions([pos_other1, pos_me1])
        pos_other2 = copy.deepcopy(pos_other1)
        pos_other2['piecePosition']["inPieceDistance"] = 2
        pos_me2 = copy.deepcopy(pos_me1)
        pos_me2['piecePosition']["inPieceDistance"] = 3
        self.keke.on_car_positions([pos_other2, pos_me2])
        self.assertEqual(self.keke.get_speed(), 3)

    def test_car_speeds_init(self):
        self.assertEquals(self.keke.get_car_position('red').speed, 0)
        self.assertEquals(self.keke.get_car_position('blue').speed, 0)

    def set_location(self, piece_index, distance_in_piece, lane):
        data = [{'piecePosition':
                   {'pieceIndex': piece_index,
                    'lane': {'startLaneIndex': lane, 'endLaneIndex': lane},
                    'lap': 0,
                    'inPieceDistance': distance_in_piece},
                 'angle': 0, 'id': {'color': self.mycolor, 'name': self.name}}]
        self.keke.on_car_positions(data)
