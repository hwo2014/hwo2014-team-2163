import unittest
from track import Track
from track import Location
from speed_observer import SpeedObserver
from main import CarPosition
from main import CarPositions


class TestSpeedObserver(unittest.TestCase):
    SAMPLE_TRACK = {
        'pieces': [
            {'length': 100.0},
            {'length': 100.0, 'switch': True},
            {'angle': 45.0, 'radius': 100},
            {'angle': 45.0, 'radius': 100},
            {'angle': 22.5, 'switch': True, 'radius': 200},
            {'angle': -45.0, 'radius': 100},
            {'length': 90.0}
        ],
        'lanes': [{'index': 0, 'distanceFromCenter': -10},
                  {'index': 1, 'distanceFromCenter': 10}],
      }
    CARS = [{'id': {'color': 'red'}},
            {'id': {'color': 'blue'}}]

    def setUp(self):
        self.car_positions = CarPositions(self.CARS)
        self.track = Track(self.SAMPLE_TRACK)
        self.speed_observer = SpeedObserver(self.car_positions, self.track)

    def test_fastest_saved_when_in_next_piece(self):
        fast = 6
        pos1 = Location(2, 0, 0)
        pos2 = Location(3, 0, 0)
        self.set_car_position(pos1, fast)
        self.speed_observer.update_speed_info()
        self.set_car_position(pos2, fast)
        self.speed_observer.update_speed_info()
        self.assertEqual(self.track.get_fastest_speed_seen(pos1), fast)

    def test_fastest_not_saved_until_fully_ran(self):
        fast = 6
        pos1 = Location(2, 0, 0)
        pos2 = Location(2, 0, 10)
        self.set_car_position(pos1, fast)
        self.set_car_position(pos2, fast)
        self.assertEqual(self.track.get_fastest_speed_seen(pos1), 0)

    def test_crash_speed_saved(self):
        toofast = 10
        restart_slow = 5
        pos1 = Location(2, 0, 0)
        pos2 = Location(3, 0, 10)
        self.set_car_position(pos1, toofast, color="red")
        self.speed_observer.crash("red")
        self.assertEqual(self.track.get_lowest_crash_speed_seen(pos1), toofast)
        self.set_car_position(pos1, restart_slow, color="red")
        self.set_car_position(pos2, restart_slow, color="red")
        self.assertEqual(self.track.get_fastest_speed_seen(pos1), restart_slow)

    def set_car_position(self, location, speed, angle=0, color='red'):
        self.car_positions.set_car_position(color,
            CarPosition(location, angle, speed))
        self.speed_observer.update_speed_info()
