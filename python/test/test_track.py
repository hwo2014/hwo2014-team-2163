import unittest
from track import Location
from track import Track
from track import PieceInfo
from math import pi


class TestPieceInfo(unittest.TestCase):
    def test_straight_piece(self):
        p = PieceInfo({'length': 100.0, 'switch': True}, [-10, 10])
        self.assertEqual(p.get_lane_length(0), 100)
        self.assertEqual(p.get_lane_length(1), 100)

    def test_bend_positive(self):
        p = PieceInfo({'angle': 90.0, 'radius': 100}, [-10, 10])
        self.assertEqual(p.angle, 90)
        expected_lane0_len = 2 * pi * (100 + 10) * 90 / 360  # ~172.79
        expected_lane1_len = 2 * pi * (100 - 10) * 90 / 360  # ~141.37
        self.assertAlmostEqual(p.get_lane_length(0), expected_lane0_len, 2)
        self.assertAlmostEqual(p.get_lane_length(1), expected_lane1_len, 2)

    def test_bend_negative(self):
        p = PieceInfo({'angle': -90.0, 'radius': 100}, [-10, 10])
        self.assertEqual(p.angle, -90)
        expected_lane0_len = 2 * pi * (100 - 10) * 90 / 360  # ~141.37
        expected_lane1_len = 2 * pi * (100 + 10) * 90 / 360  # ~172.79
        self.assertAlmostEqual(p.get_lane_length(0), expected_lane0_len, 2)
        self.assertAlmostEqual(p.get_lane_length(1), expected_lane1_len, 2)


class TestTrack(unittest.TestCase):
    SAMPLE_TRACK = {
        'pieces': [
            {'length': 100.0},
            {'length': 100.0, 'switch': True},
            {'angle': 45.0, 'radius': 100},
            {'angle': 45.0, 'radius': 100},
            {'angle': 22.5, 'switch': True, 'radius': 200},
            {'angle': -45.0, 'radius': 100},
            {'length': 90.0}
        ],
        'lanes': [{'index': 0, 'distanceFromCenter': -10},
                  {'index': 1, 'distanceFromCenter': 10}],
      }
    LAST_PIECE = 6

    def setUp(self):
        self.track = Track(self.SAMPLE_TRACK)

    def tearDown(self):
        pass

    def test_lane_length(self):
        self.assertEqual(self.track.get_lane_length(0, 0), 100)

    def test_lane_angle(self):
        self.assertEqual(self.track.get_lane_angle(2), 45)

    def test_distance(self):
        loc1 = Location(0, 0, 10)
        loc2 = Location(0, 0, 22)
        self.assertEqual(self.track.get_distance(loc1, loc2), 12)

    def test_distance_piece_changed(self):
        loc1 = Location(0, 0, 10)
        loc2 = Location(1, 0, 20)
        self.assertEqual(self.track.get_distance(loc1, loc2), 110)

    def test_distance_between_many_pieces(self):
        loc1 = Location(0, 0, 1)
        loc2 = Location(2, 0, 5)
        self.assertEqual(self.track.get_distance(loc1, loc2), 204)

    def test_distance_lap_change(self):
        loc1 = Location(self.LAST_PIECE, 0, 10)
        loc2 = Location(2, 0, 20)
        self.assertEqual(self.track.get_distance(loc1, loc2), 300)

    def test_get_next_location(self):
        loc1 = Location(self.LAST_PIECE, 0, 10)
        loc = self.track.get_next_piece_location(loc1, 2)
        self.assertEqual(loc.piece_index, 1)
