class RookieStrategy:
    def __init__(self, track, car_positions, my_color):
        self.track = track
        self.car_positions = car_positions
        self.my_color = my_color

    def get_throttle(self):
        my_position = self.car_positions.get_car_position(self.my_color)
        future1 = self.track.get_next_piece_location(
                                my_position.location, 1)
        future2 = self.track.get_next_piece_location(
                                my_position.location, 2)
        seen_crash_speed1 = self.track.get_lowest_crash_speed_seen(future1)
        seen_crash_speed2 = self.track.get_lowest_crash_speed_seen(future2)
        print "My speed %f index %d future 1 %f future 2 %f" % (
            my_position.speed, my_position.location.piece_index,
            seen_crash_speed1, seen_crash_speed2)
        if (my_position.speed > seen_crash_speed1 or
            my_position.speed > seen_crash_speed2):
            return 0.1
        else:
            return 0.7
#        d1, d2 = self.get_next_directions(2)
#        brake_limit = self.my_position.speed * 15
#        current_angle = abs(d1[1])
#        current_dist = d1[0]
#        next_angle = abs(d2[1])
#        next_dist = d2[0]
#        if current_dist > brake_limit and current_angle < 2:
#            return 1.0
#        elif abs(self.angle) > 10:
#            return 0.1
#        elif (current_dist < brake_limit and
#              next_angle > 10 and next_dist > 50 and
#              self.my_position.speed > 6):
#            return 0
#        return 0.7

    def get_next_directions(self, max_count):
        curr_index = self.my_position.location.piece_index
        curr_dist = self.my_position.location.distance_in_piece
        lane = self.my_position.location.lane
        d = self.track.get_lane_length(curr_index, lane)
        a = self.track.get_lane_angle(curr_index)
        d = d - curr_dist
        directions = []
        for _ in range(max_count):
            val = (d, a)
            while a == val[1]:
                curr_index = curr_index + 1
                d = self.track.get_lane_length(curr_index, lane)
                a = self.track.get_lane_angle(curr_index)
                if a == val[1]:
                    val = (val[0] + d, val[1] + a)
            directions.append(val)
        return directions
