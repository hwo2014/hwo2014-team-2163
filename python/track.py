import math


class Track:
    def __init__(self, track_data):
        self.track_data = track_data
        laneinfo = track_data["lanes"]
        self.lane_distances = [0] * len(laneinfo)
        for lane in laneinfo:
            self.lane_distances[lane["index"]] = lane["distanceFromCenter"]
        self.pieces = []
        for piece_data in track_data["pieces"]:
            self.pieces.append(PieceInfo(piece_data, self.lane_distances))

    def get_lane_length(self, piece_index, lane):
        index = piece_index % len(self.pieces)
        return self.pieces[index].get_lane_length(lane)

    def get_lane_angle(self, piece_index):
        index = piece_index % len(self.pieces)
        return self.pieces[index].angle

    def get_distance(self, start, end):
        if start == None:
            return 0
        a = start.piece_index
        b = end.piece_index
        if a == b:
            return end.distance_in_piece - start.distance_in_piece
        d = (self.pieces[start.piece_index].get_lane_length(start.lane) -
             start.distance_in_piece)
        idx = (start.piece_index + 1) % len(self.pieces)
        while idx != end.piece_index:
            d += self.pieces[idx].get_lane_length(start.lane)
            idx = (idx + 1) % len(self.pieces)
        d += end.distance_in_piece
        return d

    def get_fastest_speed_seen(self, location):
        return (self.pieces[location.piece_index].
            get_fastest_speed_seen(location.lane))

    def update_fastest_speed_seen(self, location, speed):
        (self.pieces[location.piece_index].
            update_fastest_speed_seen(location.lane, speed))

    def get_lowest_crash_speed_seen(self, location):
        return (self.pieces[location.piece_index].
                get_lowest_crash_speed_seen(location.lane))

    def update_lowest_crash_speed_seen(self, location, speed):
        (self.pieces[location.piece_index].
         update_lowest_crash_speed_seen(location.lane, speed))

    def get_next_piece_location(self, from_location, delta):
        index = (from_location.piece_index + delta) % len(self.pieces)
        return Location(index, from_location.lane, 0)


class PieceInfo:
    def __init__(self, piece_data, lane_data):
        self.lane_lengths = []
        if "length" in piece_data:
            self.angle = 0
            for _lane_dist in lane_data:
                self.lane_lengths.append(piece_data["length"])
        else:
            self.angle = piece_data["angle"]
            radius = piece_data["radius"]
            for lane_dist in lane_data:
                if self.angle > 0:
                    r = radius - lane_dist
                else:
                    r = radius + lane_dist
                # 2 * pi * r * (a/360) = pi * r * a / 180
                lane_len = math.pi * r * abs(self.angle) / 180
                self.lane_lengths.append(lane_len)
        self.fastest = [0] * len(lane_data)
        self.crash_speeds = [float('inf')] * len(lane_data)

    def get_lane_length(self, lane):
        return self.lane_lengths[lane]

    def get_fastest_speed_seen(self, lane):
        return self.fastest[lane]

    def update_fastest_speed_seen(self, lane, speed):
        if speed > self.fastest[lane]:
            self.fastest[lane] = speed

    def get_lowest_crash_speed_seen(self, lane):
        return self.crash_speeds[lane]

    def update_lowest_crash_speed_seen(self, lane, speed):
        if speed < self.crash_speeds[lane]:
            self.crash_speeds[lane] = speed


class Location:
    def __init__(self, piece_index, lane, distance_in_piece):
        self.piece_index = piece_index
        self.lane = lane
        self.distance_in_piece = distance_in_piece

    def __str__(self):
        return str(self.piece_index) + " " + str(self.lane) +\
            " " + str(self.distance_in_piece)
