import json
import socket
import sys
from track import Track
from track import Location
from strategy import RookieStrategy
from speed_observer import SpeedObserver


class KekeBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.printtime = 0
        self.my_position = None
        self.current_throttle = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        self.current_throttle = throttle

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        print("  " + repr(data))
        self.ping()

    def on_your_car(self, data):
        self.mycolor = data['color']

    def on_game_init(self, data):
        self.track = Track(data["race"]["track"])
        self.car_positions = CarPositions(data["race"]["cars"])
        self.speed_observer = SpeedObserver(self.car_positions, self.track)
        self.strategy = RookieStrategy(self.track, self.car_positions,
                                       self.mycolor)
        self.ping()
        self.log_file = open("log.txt", "w")

    def on_game_start(self, data):
        print("Race started")
        print("  " + repr(data))
        self.ping()

    def on_car_positions(self, data):
        for car_data in data:
            color = car_data["id"]["color"]
            pos_info = car_data["piecePosition"]
            old_pos = self.car_positions.get_car_position(color).location
            new_pos = Location(pos_info["pieceIndex"],
                               pos_info["lane"]["startLaneIndex"],
                               pos_info["inPieceDistance"])
            angle = car_data["angle"]
            speed = self.track.get_distance(old_pos, new_pos)
            self.car_positions.set_car_position(color,
                CarPosition(new_pos, angle, speed))
        self.speed_observer.update_speed_info()
        self.throttle(self.strategy.get_throttle())

    def on_crash(self, data):
        print("Someone crashed")
        print("  " + repr(data))
        self.speed_observer.crash(data["color"])
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print("  " + repr(data))
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            try:
                msg = json.loads(line)
                msg_type, data = msg['msgType'], msg['data']
                if msg_type in msg_map:
                    msg_map[msg_type](data)
                else:
                    self.ping()
                line = socket_file.readline()
            except Exception as ex:
                print "Exception " + repr(ex)

    def get_car_position(self, color):
        return self.car_positions.get_car_position(color)

    def get_speed(self):
        return self.car_positions.get_car_position(self.mycolor).speed

    def _calculate_speed(self, old_pos, new_pos):
        if old_pos == None:
            return 0
        return self.track.get_distance(old_pos, new_pos)


class CarPosition:
    def __init__(self, location, angle, speed):
        self.location = location
        self.angle = angle
        self.speed = speed


class CarPositions:
    def __init__(self, car_data):
        self.car_map = {}
        for car in car_data:
            l = Location(0, 0, 0)
            self.car_map[car['id']['color']] = CarPosition(l, 0, 0)

    def get_car_position(self, color):
        return self.car_map[color]

    def set_car_position(self, color, car_position):
        self.car_map[color] = car_position

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(
            *sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = KekeBot(s, name, key)
        bot.run()
